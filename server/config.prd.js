const express = require('express');
const next = require('next');
const { createProxyMiddleware } = require("http-proxy-middleware");
const kollection = require('./kollection.json');

// const { kollection, kollecties } = require('../src/kollection');
const proxy = {
  '/api': {
    target: 'http://35.190.235.222',
    pathRewrite: {'/api': '/api'},
    changeOrigin: true,
    secure: false,
  },
};

const port = parseInt(process.env.PORT, 10) || 80;
const app = next({ dev: false });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  Object.keys(proxy).forEach((context) => server.use(createProxyMiddleware(context, proxy[context])));
  server.get(`/${process.env.BASE_PATH}/scheme`, (req, res) => res.json(kollection));
  server.all('*', (req, res) => handle(req, res));

  server.listen(port, () => {
    console.log(`> Ready on port ${port}`);
  })
  .on('error', error => {
    throw error;
  });
});
