import { Close, NavigateNext } from '@mui/icons-material';
import { Box, Button, Divider, IconButton, Modal, TextField, Typography, useTheme } from '@mui/material';
import { dialogUtil } from '@nara/prologue';
import React, { useState } from 'react';


const ResetPasswordDialogContainer =
  ({
     isOpen,
     onClose,
   }: {
    isOpen: boolean,
    onClose: () => void,
  }) => {
    //
    const title = '비밀번호 초기화';

    const theme = useTheme();
    const [state, setState] = useState({
      resetPasswordStep: 1,
      inputEmail: '',
      inputCode: '',
      inputNewPassword: '',
      inputRePassword: '',
      isSame: false,
    });

    const onClickClose = () => {
      //
      onClose();

      setState({
        ...state,
        resetPasswordStep: 1,
        inputEmail: '',
        inputCode: '',
        inputNewPassword: '',
        inputRePassword: '',
        isSame: false,
      });
    };

    const onChangeEmail = (event: any) => {
      setState({ ...state, inputEmail: event.target.value });
    };

    const onChangeCode = (event: any) => {
      setState({ ...state, inputCode: event.target.value });
    };

    const onChangeNewPassword = (event: any) => {
      setState({
        ...state,
        inputNewPassword: event.target.value,
        isSame: state.inputRePassword === event.target.value,
      });
    };

    const onChangeRePassword = (event: any) => {
      setState({
        ...state,
        inputRePassword: event.target.value,
        isSame: state.inputNewPassword === event.target.value,
      });
    };

    const onEnter = async (event: any) => {
      if (event.key === 'Enter') {
        await sendEmail();
      }
    };

    const onClickNext = async () => {
      await sendEmail();
    };

    const sendEmail = async () => {
    };

    const sendEmailAfterHook = async (isSuccess: boolean, resultMessage: string) => {
      if (isSuccess) {
        await dialogUtil.alert(resultMessage, { title });
        setState({ ...state, resetPasswordStep: 2 });
      } else {
        await dialogUtil.alert(resultMessage, { title });
      }
    };

    const onClickResetPassword = async () => {
    };

    const resetPasswordAfterHook = async (isSuccess: boolean) => {
      //
      const { inputEmail } = state;

      if (isSuccess) {
        await dialogUtil.alert(`${inputEmail} 계정의 비밀번호가 변경되었습니다.`, { title });
        onClickClose();
      } else {
        await dialogUtil.alert('비밀번호를 변경할 수 없습니다. 다시 시도해주세요.', { title });
      }
    };

    const { resetPasswordStep, inputEmail, inputCode, inputNewPassword, inputRePassword, isSame } = state;

    return (
      <Modal
        disablePortal
        disableEnforceFocus
        disableAutoFocus
        open={isOpen}
        style={{
          display: 'flex',
          padding: theme.spacing(1),
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <div
          style={{
            width: 515,
            backgroundColor: theme.palette.background.paper,
            borderRadius: theme.spacing(1),
            boxShadow: theme.shadows[5],
          }}
        >
          <Box pt={2} pb={2} pl={4} pr={2} display="flex" alignItems="center" justifyContent="space-between">
            <Typography variant="h5">비밀번호 재설정</Typography>
            <IconButton onClick={onClickClose}><Close/></IconButton>
          </Box>
          <Divider/>
          {/*비밀번호 재설정-Step01 S */}
          {
            resetPasswordStep === 1 ?
              <Box ml={4} mr={4} mt={4} mb={4} flexDirection="column">
                <Box display="flex" alignItems="center">
                  <img src={`/${process.env.NEXT_PUBLIC_BASE_PATH}/images/pw-icon01.svg`}/>
                  <Box ml={3}>
                    <Typography
                      variant="h4"
                      style={{
                        fontSize: '22px',
                        lineHeight: '28px',
                      }}
                    >
                      나라 플랫폼 계정의 <br/>비밀번호를 재설정 합니다.
                    </Typography>
                    <Box height="10px"/>
                    <Typography
                      variant="body2"
                      style={{
                        fontSize: '14px',
                        color: '#666',
                      }}
                    >
                      비밀번호를 재설정할 이메일 주소를 입력해 주세요.
                    </Typography>
                  </Box>
                </Box>
                <Box mt={7}>
                  <TextField
                    autoFocus
                    size="small"
                    required
                    placeholder="example@nextree.io"
                    fullWidth
                    label="Email Address"
                    variant="outlined"
                    onChange={onChangeEmail}
                    onKeyPress={onEnter}
                  />
                </Box>
                <Box mt={7}>
                  <Button
                    fullWidth
                    size="large"
                    variant="contained"
                    color="primary"
                    endIcon={<NavigateNext/>}
                    style={{
                      padding: '10px 0 10px 0',
                      fontSize: '16px !important',
                      fontWeight: 'bold',
                    }}
                    onClick={onClickNext}
                    disabled={!inputEmail.length}
                  >
                    다음
                  </Button>
                </Box>
              </Box> : null
          }
          {/*비밀번호 재설정-Step01 E */}
          {/*비밀번호 재설정-Step02 S */}
          {
            resetPasswordStep === 2 ?
              <Box ml={4} mr={4} mt={10} mb={10} flexDirection="column">
                <Box display="flex" alignItems="center">
                  <img src={`/${process.env.NEXT_PUBLIC_BASE_PATH}/images/pw-icon02.svg`}/>
                  <Box ml={3}>
                    <Typography
                      variant="h4"
                      style={{
                        fontSize: '22px',
                        lineHeight: '28px',
                      }}
                    >비밀번호 재설정을 위해 <br/>사용자 확인을 진행합니다.
                    </Typography>
                    <Box height="10px"/>
                    <Typography
                      variant="body2"
                      style={{
                        fontSize: '14px',
                        color: '#666',
                      }}
                    >
                      발송해 드린 메일의 인증번호를 입력해 주세요.
                    </Typography>
                  </Box>
                </Box>
                <Box mt={5} display="flex" alignItems="center">
                  <TextField
                    required
                    autoFocus
                    size="small"
                    fullWidth
                    label="인증번호"
                    variant="outlined"
                    onChange={onChangeCode}
                  />
                </Box>
                <Box height="40px"/>
                <Box display="flex" alignItems="center">
                  <img src={`/${process.env.NEXT_PUBLIC_BASE_PATH}/images/pw-icon03.svg`}/>
                  <Box ml={3}>
                    <Typography
                      variant="h4"
                      style={{
                        fontSize: '22px',
                        lineHeight: '28px',
                      }}
                    >
                      새로운 비밀번호를 <br/>입력해 주세요.
                    </Typography>
                    <Box height="10px"/>
                    <Typography
                      variant="body2"
                      style={{
                        fontSize: '14px',
                        color: '#666',
                      }}
                    >
                      다른 사이트와 동일하거나 쉬운 비밀번호는 사용하지 마세요.
                    </Typography>
                  </Box>
                </Box>
                <Box mt={7} display="flex" alignItems="center">
                  <TextField
                    required
                    size="small"
                    fullWidth
                    label="새 비밀번호 입력"
                    type="password"
                    variant="outlined"
                    onChange={onChangeNewPassword}
                  />
                </Box>
                <Box mt={2}>
                  <TextField
                    required
                    size="small"
                    fullWidth
                    label="비밀번호 재입력"
                    variant="outlined"
                    type="password"
                    onChange={onChangeRePassword}
                  />
                  {
                    inputRePassword.length && (
                      isSame ?
                        <Typography
                          variant="caption"
                          style={{
                            paddingTop: theme.spacing(1),
                            paddingLeft: theme.spacing(1),
                            color: 'green',
                          }}
                        >
                          일치합니다.
                        </Typography> :
                        <Typography
                          variant="caption"
                          style={{
                            paddingTop: theme.spacing(1),
                            paddingLeft: theme.spacing(1),
                            color: 'red',
                          }}
                        >
                          일치하지 않습니다.
                        </Typography>
                    ) || null
                  }
                </Box>
                <Box mt={7}>
                  <Button
                    fullWidth
                    size="large"
                    variant="contained"
                    color="primary"
                    style={{
                      padding: '10px 0 10px 0',
                      fontSize: '16px !important',
                      fontWeight: 'bold',
                    }}
                    disabled={!(inputNewPassword.length && inputRePassword.length && isSame && inputCode.length > 4)}
                    onClick={onClickResetPassword}
                  >
                    비밀번호 재설정
                  </Button>
                </Box>
              </Box> : null
          }
        </div>
      </Modal>
    );
  };

export default ResetPasswordDialogContainer;
