enum TapCategory {
  //
  Servant = 'Servant',
  Scope = 'Scope',
  Tier = 'Tier',
}

export default TapCategory;
