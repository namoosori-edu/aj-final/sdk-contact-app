import Menu from './Menu';

class Breadcrumb {
  //
  menu: Menu;

  constructor(menu: Menu) {
    //
    this.menu = menu;
  }
}

export default Breadcrumb;
