enum MenuType {
  //
  Menu = 'Menu',
  Title = 'Title',
  Action = 'Action',
  Hidden = 'Hidden',
}

export default MenuType;
