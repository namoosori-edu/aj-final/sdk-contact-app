import { useDock } from '@nara/dock';
import { useRouter } from 'next/router';
import { PropsWithChildren } from 'react';
import { usePage } from './provider';

const PageLayoutContainer =
  ({
     children,
   }: PropsWithChildren<{}>) => {
    //
    const [page] = usePage();

    // if withoutAuth, skip check allowed page url
    if (page.withoutAuth) {
      return (
        <>
          {children}
        </>
      );
    }

    // check allowed page url from dock context data
    const router = useRouter();
    const dock = useDock();
    const { currentKollection, currentStage } = dock;

    // if no kollection matched and with, skip render
    const kollection = currentStage?.kollections.find((kollection: any) => kollection.path === currentKollection?.path);
    // if (!kollection) {
    //   return null;
    // }

    // if no kollectie matched, skip render
    const kollectie = kollection?.kollecties.find((kollectie: any) => router.pathname.includes(`/${kollectie.path}`));
    // if (!kollectie) {
    //   return null;
    // }

    return (
      <>
        {children}
      </>
    );
  };

export default PageLayoutContainer;
