import { Kollectie } from '@nara/accent';
import { useAuth, useDock } from '@nara/dock';
import React, { PropsWithChildren, useContext, useEffect, useState } from 'react';
import { Breadcrumb, Menu } from '../../../model';
import { development } from '~/devconfig';
import kollection from '~/../server/kollection.json';
import { ForbiddenPage } from '~/app';


export interface Page {
  //
  title: string;
  breadcrumbs: Breadcrumb[];
  kollectie?: Kollectie;
  menu?: Menu;
  withoutAuth?: boolean;
}

export type UsePage = [
  //
  Page,
  React.Dispatch<React.SetStateAction<Page>>,
];

export const PageContext = React.createContext<UsePage>(undefined!);

export const usePage = () => useContext(PageContext);

const { kollecties } = kollection;

const PageProvider =
  ({
     children,
     loginUrl,
     withoutAuth = false,
   }: PropsWithChildren<{
    loginUrl: string,
    withoutAuth?: boolean,
  }>) => {
    //

    // if with auth and no login, redirect to login url
    const auth = useAuth();
    const dock = useDock();
    useEffect(() => {
      if (development) {
        // skip when development mode
        return;
      }

      const key = 'nara.accessToken';
      const token = window.sessionStorage.getItem(key) || window.localStorage.getItem(key);

      if (!withoutAuth && !auth.loggedIn && !token) {
        window.location.href = loginUrl;
      }
    }, [auth]);

    // set page context to use title, breadcrumb, withoutAuth data
    const [page, setPage] = useState<Page>({
      title: '',
      breadcrumbs: [],
      withoutAuth: withoutAuth,
    });

    if (!withoutAuth && !auth.loggedIn) {
      return null;
    }

    if (page?.kollectie && dock?.loaded) {
      const requiredRoles = kollecties.find(item => item.path === page?.kollectie?.path)?.requiredRoles || [];
      const currentStageRoles = dock.currentStageRoles || [];
      console.log('currentStageRoles', currentStageRoles);

      if (requiredRoles.length > 0) {
        const hasRole = requiredRoles.some(role => currentStageRoles.includes(role));

        if (!hasRole) {
          return (
            <ForbiddenPage roles={requiredRoles} />
          );
        }
      }

      console.log('requiredRoles', requiredRoles);
    }

    return (
      <PageContext.Provider value={[page, setPage]}>
        {children}
      </PageContext.Provider>
    );
  };

export default PageProvider;
