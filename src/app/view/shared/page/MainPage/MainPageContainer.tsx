import { Box, Button, Container, Grid, Link, Toolbar, Typography } from '@mui/material';
import React, { useEffect } from 'react';
// import { observer } from 'mobx-react';
// import { AppFlowStateKeeper, Cast, StageContextFlowStateKeeper } from '@nara.drama/stage';
// import { CastCard, PageLayout } from '~/comp/view';


const MainPageContainer =
  ({
     isLogin,
     isActor,
     onClickLogin,
     onClickLogout,
     onClickSignUp,
   }: {
    isLogin: boolean,
    isActor: boolean,
    onClickLogin: () => void,
    onClickLogout: () => void,
    onClickSignUp: () => void,
  }) => {
    //
    useEffect(() => {
      //
      // const { stageContextFlowStateKeeper } = this.injected;
      // const { currentAudience, refreshContext, currentAudienceId } = stageContextFlowStateKeeper;
      //
      // if ( currentAudience ) {
      //   refreshContext()
      //     .then(() => this.findAccessibleCasts(currentAudienceId));
      // }
    });

    const findAccessibleCasts = (audienceId: string) => {
      //
      // const { appFlowStateKeeper } = this.injected;
      //
      // appFlowStateKeeper.findActorsByAudienceId(audienceId)
      //   .then(actors => appFlowStateKeeper.findCastsByIds(actors.map(actor => actor.castId)));
    };

    // const onClickCast = (cast: Cast) => {
    //   const { stageContextFlowStateKeeper } = this.injected;
    //
    //   stageContextFlowStateKeeper.switchContext(cast.id)
    //     .then(() => window.location.href = cast.basePath);
    // };

    // const { casts } = this.injected.appFlowStateKeeper;

    return (
      <Box>
        <Toolbar>
          <Box pl={3}>
            <img src="/broadcast-organizer/images/logo/namoosori_logo.png" alt=""/>
          </Box>
          <Box width="100%" justifyContent="flex-end" pr={3} display="flex">
            {isLogin ?
              <Button variant="text" onClick={onClickLogout}>LOGOUT</Button>
              :
              <>
                {/*<Box mr={3}>*/}
                {/*  <Button variant="text" onClick={onClickLogin}>LOGIN</Button>*/}
                {/*</Box>*/}
                <Button variant="text" onClick={onClickSignUp}>SIGN UP</Button>
                <Button variant="text" onClick={onClickLogin}>LOGIN</Button>
              </>
            }
          </Box>
        </Toolbar>

        <Container>
          <Box mb={8}>
            <Grid container spacing={10}>
              {/*<Grid container item xs={12}>*/}
              {/*  <Grid item xs={12}>*/}
              {/*    <CastCard*/}
              {/*      casts={casts}*/}
              {/*      onClickCast={this.onClickCast}*/}
              {/*    />*/}
              {/*  </Grid>*/}
              {/*</Grid>*/}
              <Grid item xs={12} sm={6}>
                <Box>
                  <img src="/broadcast-organizer/images/main/namoosori_02.png" alt=""/>
                </Box>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Box>
                  <Typography paragraph variant="h1">Nara Way</Typography>
                  <Typography variant="body1">
                    이 앱을 통해 효율적으로 방송을 편성하세요 <br/>
                    조직 구성부터 최적화된 작업 화면까지 각자의 역할에 맞추어 해야할 일만 효율적으로 관리하세요.
                  </Typography>
                </Box>
                <Box width="100%" textAlign="left" pt={3}>
                  <Button variant="text">
                    <Link href="https://www.nextree.io/namoosori/"><a>VIEW DETAIL</a></Link>
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
    );
  };


export default MainPageContainer;
