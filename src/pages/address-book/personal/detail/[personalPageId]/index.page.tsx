import { useRouter } from 'next/router';
import { Box, Button } from '@mui/material';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { PersonalAddressPageDetail } from '@nara.drama/address-book';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};
export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

const TemplatePage = () => {
  //
  const router = useRouter();
  const personalPageId = router.query.personalPageId as string;

  const handleClickModify = () => router.push(`/address-book/personal/modify/${personalPageId}`);
  const handleClickCancel = () => router.back();

  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <PersonalAddressPageDetail
          addressPageId={personalPageId}
        />
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center'
          }}
        >
          <Button onClick={handleClickModify} variant={'contained'} size={'large'}>수정</Button>
          <Button onClick={handleClickCancel} variant={'outlined'} size={'large'} sx={{ml: 2}}>취소</Button>
        </Box>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
