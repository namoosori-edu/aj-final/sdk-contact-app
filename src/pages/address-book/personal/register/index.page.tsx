import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { PersonalAddressPageForm } from '@nara.drama/address-book';
import { useDock } from '@nara/dock';
import { useRouter } from 'next/router';
import { dialogUtil } from '@nara/prologue';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};

const TemplatePage = () => {
  //
  const dock = useDock();
  const router = useRouter();

  if (!dock.currentCitizen?.id) {
    return null;
  }

  const handleClickCancel = () => router.back();
  const handleSuccess = () => router.push('/address-book/personal');
  const handleFail = () => dialogUtil.alert('등록하지 못했습니다.', { title: '개인 주소록' });

  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <PersonalAddressPageForm
          addressBookId={dock.currentCitizen?.id || '1@1:1:1:1'}
          onClickCancel={handleClickCancel}
          onSuccess={handleSuccess}
          onFail={handleFail}
        />
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
