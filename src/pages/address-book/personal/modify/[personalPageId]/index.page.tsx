import { useRouter } from 'next/router';
import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { PersonalAddressPageForm } from '@nara.drama/address-book';
import { dialogUtil } from '@nara/prologue';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: false,
    },
  };
};
export const getStaticPaths = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  };
}

const TemplatePage = () => {
  //
  const router = useRouter();
  const personalPageId = router.query.personalPageId as string;

  const handleClickCancel = () => router.back();
  const handleSuccess = () => router.push(`/address-book/personal/detail/${personalPageId}`);
  const handleFail = () => dialogUtil.alert('수정하지 못했습니다.', { title: '개인 주소록' });

  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <PersonalAddressPageForm
          addressPageId={personalPageId}
          onClickCancel={handleClickCancel}
          onSuccess={handleSuccess}
          onFail={handleFail}
        />
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
