import { PageLayout, PageLayoutContent, PageLayoutHeader } from '~/app';
import { AddressPage, PersonalAddressPageList } from '@nara.drama/address-book';
import { useDock } from '@nara/dock';
import { useRouter } from 'next/router';
import { Box, Button } from '@mui/material';

export const getStaticProps = async () => {
  return {
    props: {
      withoutAuth: true,
    },
  };
};

const TemplatePage = () => {
  //
  const dock = useDock();
  const router = useRouter();

  if (!dock.currentCitizen?.id) {
    return null;
  }

  const handleClickRegister = () => router.push('/address-book/personal/register');
  const handleClickAddressPage = (addressPage: AddressPage) => router.push(`/address-book/personal/detail/${addressPage.id}`);

  return (
    <PageLayout>
      <PageLayoutHeader title="개인 주소록"/>
      <PageLayoutContent>
        <PersonalAddressPageList
          addressBookId={dock.currentCitizen?.id || '1@1:1:1:1'}
        >
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              justifyContent: 'flex-end'
            }}
          >
            <Button onClick={handleClickRegister} variant={'contained'} size={'large'}>등록</Button>
          </Box>
          <PersonalAddressPageList.Sort />
          <PersonalAddressPageList.Content
            onClickAddressPage={handleClickAddressPage}
          />
        </PersonalAddressPageList>
      </PageLayoutContent>
    </PageLayout>
  );
};

export default TemplatePage;
