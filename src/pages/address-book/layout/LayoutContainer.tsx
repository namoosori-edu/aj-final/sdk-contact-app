import { Box, ThemeProvider } from '@mui/material';
import { Kollectie } from '@nara/accent';
import { CurrentKollection, useDock } from '@nara/dock';
import { useRouter } from 'next/router';
import React, { PropsWithChildren, useEffect } from 'react';
import { HeaderLayout, Menu, MenuType, SideLayout, theme as baseTheme, usePage } from '~/app';


export const kollectie = 'address-book';
export const menus: Menu[] = [
  Menu.newTitle('주소록', [
    Menu.newMenu(`/${kollectie}/personal`, '개인 주소록', `/${kollectie}/personal`),
    Menu.newMenu(`/${kollectie}/team`, '단체 주소록', `/${kollectie}/team`),
  ]),
];
const menuItems = (menus: Menu[]): Menu[] => {
  const items: Menu[] = [];
  menus.forEach(menu => {
    items.push(menu);
    if (menu.children) {
      items.push(...menuItems(menu.children));
    }
  });
  return items;
};

const LayoutContainer = ({ children }: PropsWithChildren<{}>) => {
  //
  const dock = useDock();
  const [page, setPage] = usePage();
  const router = useRouter();

  useEffect(() => {
    //
    setPageProps();
  }, [router.asPath]);

  const handleClickMenu = (menu: Menu) => {
    //
    if (menu.type === MenuType.Menu && menu.href) {
      router.push(menu.href || '', menu.as || menu.href);
    } else if (menu.type === MenuType.Action && menu.action) {
      menu.action();
    }
  };

  const handleClickHome = () => {
    //
    router.push('/');
  };

  const setPageProps = () => {
    //
    const kollection = new CurrentKollection(
      'contact',
      'Contact',
      'contact',
    );
    const kollectie = new Kollectie(
      'address-book',
      '주소록',
      '',
      [],
    );

    const breadcrumbs = Menu.breadcrumb(kollection, kollectie, menus, router.pathname);

    if (breadcrumbs.length > 0) {
      setPage({
        ...page,
        kollectie,
        title: breadcrumbs[breadcrumbs.length - 1].menu.title,
        breadcrumbs,
        menu: breadcrumbs[breadcrumbs.length - 1].menu,
      });
    }
  };

  return (
    <ThemeProvider theme={baseTheme}>
      <Box display="flex">
        <Box flexGrow={1} style={{ marginLeft: '240px' }}>
          <HeaderLayout
            activeKollection={dock.currentKollection?.path || ''}
            activeKollectie={kollectie}
          />
          <Box sx={{backgroundColor: 'white'}}>
            {children}
          </Box>
        </Box>
        {menus.length > 0 && (
          <SideLayout
            menus={menus}
            onClickMenu={handleClickMenu}
            onClickLogo={handleClickHome}
          />
        )}
      </Box>
    </ThemeProvider>
  );
}

export default LayoutContainer;
