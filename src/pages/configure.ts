import { AxiosApi } from '@nara/prologue';
import moment from 'moment';
import { development } from '../devconfig';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/ko'

// @ts-ignore
import packageJson from '../../package.json';
import { Interceptor } from "@nara/dock";
import dayjs from 'dayjs';

export const configure = () => moment.locale('ko');
dayjs.locale('ko');
dayjs.extend(relativeTime);

AxiosApi.setUrlRewrite({
  '/api/checkpoint': '/api/checkpoint',
  '/api/town': '/api/town',
  '/api/gallery': '/api/gallery',
  '/api/voice': '/api/voice',
});

export const dramaRoleConfig: { [key: string]: string } = {
  '/api/town/': 'town',
  '/api/gallery/': 'gallery',
  '/api/address-book/': 'address-book',
};

export const dramaRolesInterceptors: Interceptor[] = [{
  request: {
    onFulfilled: (config) => {
      const key = 'nara.currentDramaRoles';
      const currentDramaRoles = window.sessionStorage.getItem(key) || window.localStorage.getItem(key);
      const url = config?.url || '';

      if (currentDramaRoles && dramaRoleConfig && Object.keys(dramaRoleConfig).some(key => url.startsWith(key))) {
        const dramaRoles = JSON.parse(currentDramaRoles) as string[];
        const drama = dramaRoleConfig[Object.keys(dramaRoleConfig).find(key => url.startsWith(key)) || ''];
        const roles = dramaRoles
          .filter(role => role.startsWith(`${drama}:`))
          .map(role => role.substring(role.indexOf(':') + 1)).join(',');

        if ((console as any).debugging) console.debug('[configure:dramaRolesInterceptors] currentDramaRoles exist, url =', url, 'roles =', roles);
        const prevHeaders = config.headers || {};
        const headers = { ...prevHeaders, roles };
        config.headers = headers;
      } else {
        if ((console as any).debugging) console.debug('[configure:dramaRolesInterceptors] currentDramaRoles is empty or url not match');
      }
      return config;
    },
  },
}];

if (development) {
  console.log(`
%c${packageJson.description} Micro-App

%cCopyright (c) NEXTREE Inc. @since 2014. 6. 10.

💡️ Does you find some bug or improvements?
📮 Add your feedback to nara way dev team!
  `,
    'font-family:"Courier New";font-size:1.6em;font-weight:bold',
    '',
  );
}
