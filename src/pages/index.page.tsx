import { useAuth, useDock } from '@nara/dock';
import { useRouter } from 'next/router';
import { GetServerSidePropsContext } from 'next/types';
import * as React from 'react';
import { useEffect } from 'react';
import { development } from '../devconfig';


export const getServerSideProps = async ({ res }: GetServerSidePropsContext) => {
  //
  res.setHeader(
    'Cache-Control',
    'no-cache',
    // 'public, s-maxage=10, stale-while-revalidate=59',
  );

  return {
    props: {
      withoutAuth: true,
      withoutLayout: true,
    }
  };
};

const Index = () => {
  //
  const noappUrl = development ? `/${process.env.NEXT_PUBLIC_BASE_PATH}/default/noapp` : process.env.NEXT_PUBLIC_NOAPP_PATH || '';

  const auth = useAuth();
  const dock = useDock();
  const router = useRouter();

  useEffect(() => {
    //
    const key = 'nara.accessToken';
    const token = window.sessionStorage.getItem(key) || window.localStorage.getItem(key);

    console.log('auth', auth);
    if (!auth.loggedIn && !token) {
      router.push('/default');
      return;
    }

    const kollection = dock?.currentKollection;
    console.log('kollection', kollection);
    const kollections = dock?.currentStage ? dock?.currentStage.kollections : [];

    if (!dock || !kollection || !kollections || kollections.length === 0) {
      return;
    }

    const kollecties = kollections.find(item => item.path === kollection.path)?.kollecties || [];
    console.log('kollecties', kollecties);
    if (!kollecties || kollecties.length === 0) {
      if (!kollections || kollections.length === 0 || !kollections[0].kollecties || kollections[0].kollecties.length === 0) {
        window.location.href = noappUrl;
        return;
      }

      kollecties.push(...kollections[0].kollecties);
      dock.switchContext(`/${kollections[0].path}/${kollecties[0].path}`);
      dock.reload();
      window.location.href = `/${kollections[0].path}/${kollecties[0].path}`;
    } else {
      window.location.href = `/${kollection.path}/${kollecties[0].path}`;
    }
  }, [auth, dock]);

  return null;
}

export default Index;
